#ifndef RENDERER_H
#define RENDERER_H


#include <vector>
#include <cairomm/context.h>
#include <iostream>

#include "math.h"
#include "xcb_draw.h"

class Triangle
{
    public:
        Triangle(Vector3 *verts);
        virtual ~Triangle();

        Vector3 vertices[3];
        Vector3 normal;
        void print();
};

class Mesh
{
    public:
        Mesh(std::vector<Triangle> triangles);
        virtual ~Mesh();

        std::vector<Triangle> triangles;
        void print();
};

class World
{
    public:
        World();
        virtual ~World();
        std::vector<Mesh> meshes;
};

class Renderer
{
    public:
        Renderer(int width, int height, XcbDraw &draw);
        virtual ~Renderer();

        World world;
        void render();
        int width;
        int height;
        XcbDraw draw;

    private:
        Matrix<4, 4> projection;
        void draw_triangle(std::vector<Vector4> &vec, uint8_t shade);

};

Vector4 transform_vector(Matrix<4, 4> &trans, Vector3 &vec);
Vector4 to_2d(Matrix<4, 4> &projection, Vector4 &vec);

#endif
