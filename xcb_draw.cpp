#include "xcb_draw.h"


XcbDraw::XcbDraw() {

    uint32_t value_mask;
    uint32_t value_list[2];

    //connect to the X server and get screen

    connection = xcb_connect(NULL, NULL);
    screen = xcb_setup_roots_iterator(xcb_get_setup(connection)).data;

    //create a window

    value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    value_list[0] = screen->black_pixel;
    value_list[1] = XCB_EVENT_MASK_EXPOSURE;

    window = xcb_generate_id(connection);

    xcb_create_window(
        connection,            
        screen->root_depth,  
        window,    
        screen->root,    
        0, 0,         
        WIDTH, HEIGHT,    
        0,              
        XCB_WINDOW_CLASS_INPUT_OUTPUT,   
        screen->root_visual,    
        value_mask, value_list  
    );

    //create a graphic context

    value_mask = XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES;
    value_list[0] = screen->black_pixel;
    value_list[1] = 0;

    gcontext = xcb_generate_id(connection);
    xcb_create_gc(connection, gcontext, window, value_mask, value_list);

    //map the window onto the screen

    xcb_map_window(connection, window);
    xcb_flush(connection);


    //Shm test
    xcb_shm_query_version_reply_t*  reply;

    reply = xcb_shm_query_version_reply(
        connection,
        xcb_shm_query_version(connection),
        NULL
    );

    if (!reply || !reply->shared_pixmaps){
        printf("Shm error...\n");
        std::cout << reply->shared_pixmaps << std::endl;
        //exit(0);
    }

    info.shmid   = shmget(IPC_PRIVATE, WIDTH * HEIGHT * 4, IPC_CREAT | 0600);
    info.shmaddr = (uint8_t*)shmat(info.shmid, 0, 0);

    info.shmseg = xcb_generate_id(connection);
    xcb_shm_attach(connection, info.shmseg, info.shmid, 0);

    data = info.shmaddr;

    pix = xcb_generate_id(connection);
    xcb_shm_create_pixmap(
        connection,
        pix,
        window,
        WIDTH, HEIGHT,
        screen->root_depth,
        info.shmseg,
        0
    );

    zbuf = new int16_t[WIDTH * HEIGHT];
}

XcbDraw::~XcbDraw() 
{
    shmctl(info.shmid, IPC_RMID, 0);

    xcb_shm_detach(connection, info.shmseg);
    shmdt(info.shmaddr);

    xcb_free_pixmap(connection, pix);

    xcb_destroy_window(connection, window);
    xcb_disconnect(connection);
}

void XcbDraw::draw()
{
#define CHUNKS 8

    //#pragma omp parallel for
    for (int i = 0; i < CHUNKS; i++) {
        xcb_copy_area(
                connection,
                pix,
                window,
                gcontext,
                i * (WIDTH / CHUNKS), 0, i * (WIDTH / CHUNKS), 0,
                WIDTH / CHUNKS, HEIGHT
                );
    }

    xcb_flush(connection);
}

void XcbDraw::clear(uint8_t shade)
{
    //#pragma omp parallel for
    /*
    for (int i = 0; i < WIDTH * HEIGHT * 4; i++) {
        data[i] = shade;
        if (i % 4 == 0) zbuf[i / 4] = INT32_MAX;
    }
    */

    std::fill(data, data + WIDTH * HEIGHT * 4, shade);
    std::fill(zbuf, zbuf + WIDTH * HEIGHT, INT16_MAX);
}
