#ifndef MATH_H
#define MATH_H

#include <iostream>
#include <assert.h>
#include <cmath>
#include <algorithm>

template <int n, int m>
class Matrix 
{
    public:
        Matrix();
        Matrix(double x, double y, double z);
        Matrix(double x, double y, double z, double w);
        Matrix(const Matrix<4, 1> &other);
        Matrix(const Matrix<3, 1> &other);
        virtual ~Matrix();

        void print();
        double m_matrix[n][m]; 

        template<int l>
        Matrix<n, l> operator * (const Matrix<m, l> &other) const;
        Matrix<n, m> operator * (double scale) const;

        Matrix<n, m> operator + (const Matrix<n, m> &other) const;
        Matrix<n, m> operator - (const Matrix<n, m> &other) const;

        Matrix<m, n> transpose();

        static const Matrix<4, 4> rot_x(double theta);
        static const Matrix<4, 4> rot_y(double theta);
        static const Matrix<4, 4> rot_z(double theta);

        static const Matrix<4, 4> translation(const Matrix<3, 1> &vec);
        static const Matrix<4, 4> scale(const Matrix<3, 1> &vec);

        double x();
        double y();
        double z();
        double w();
        double size();

        double dot(Matrix<n, m> &other);
        Matrix<n, 1> cross(Matrix<n, 1> &other);

        double& operator[] (int index);

    protected:
};

template <int n, int m>
Matrix<n, m>::Matrix()
{
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            m_matrix[i][j] = 0;
        }
    }
}

template <int n, int m>
Matrix<n, m>::~Matrix()
{
}

template <int n, int m>
void Matrix<n, m>::print() 
{
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            std::cout << m_matrix[i][j] << " ";
        }

        std::cout << std::endl;
    }
}

template <int n, int m>
Matrix<m, n> Matrix<n, m>::transpose()
{
    Matrix<m, n> result;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            result.m_matrix[j][i] = m_matrix[i][j];
        }
    }

    return result;
}

template <int n, int m>
Matrix<n, m>::Matrix(double x, double y, double z)
{
    m_matrix[0][0] = x;
    m_matrix[1][0] = y; 
    m_matrix[2][0] = z; 
}

template <int n, int m>
Matrix<n, m>::Matrix(double x, double y, double z, double w)
{
    m_matrix[0][0] = x;
    m_matrix[1][0] = y; 
    m_matrix[2][0] = z; 
    m_matrix[3][0] = w; 
}

template <int n, int m>
Matrix<n, m>::Matrix(const Matrix<4, 1> &other)
{
    m_matrix[0][0] = other.m_matrix[0][0];
    m_matrix[1][0] = other.m_matrix[1][0]; 
    m_matrix[2][0] = other.m_matrix[2][0]; 
    m_matrix[3][0] = other.m_matrix[3][0]; 
}

template <int n, int m>
Matrix<n, m>::Matrix(const Matrix<3, 1> &other)
{
    m_matrix[0][0] = other.m_matrix[0][0];
    m_matrix[1][0] = other.m_matrix[1][0]; 
    m_matrix[2][0] = other.m_matrix[2][0]; 
}


template <int n, int m>
double Matrix<n, m>::x() { return m_matrix[0][0]; }
template <int n, int m>
double Matrix<n, m>::y() { return m_matrix[1][0]; }
template <int n, int m>
double Matrix<n, m>::z() { return m_matrix[2][0]; }
template <int n, int m>
double Matrix<n, m>::w() { return m_matrix[3][0]; }

template <int n, int m>
double Matrix<n, m>::size()
{
    return sqrt(this->dot(*this));
}

template <int n, int m>
double Matrix<n, m>::dot(Matrix<n, m> &other)
{
    return (transpose() * other).m_matrix[0][0];
}

template <int n, int m>
Matrix<n, 1> Matrix<n, m>::cross(Matrix<n, 1> &other)
{
    return Matrix<n, m>(
        y() * other.z() - z() * other.y(),
        z() * other.x() - x() * other.z(),
        x() * other.y() - y() * other.x(),
        w()
    );
}

template <int n, int m>
double& Matrix<n, m>::operator[] (int index)
{
    return m_matrix[index][0];
}

template <int n, int m>
template <int l>
Matrix<n, l> Matrix<n, m>::operator * (const Matrix<m, l> &other) const
{
    Matrix<n, l> result;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < l; j++) {
            result.m_matrix[i][j] = 0;
            for (int k = 0; k < m; k++) {
                result.m_matrix[i][j] += m_matrix[i][k] * other.m_matrix[k][j];
            }
        }
    }

    return result;
}

template <int n, int m>
Matrix<n, m> Matrix<n, m>::operator * (double scale) const
{
    Matrix<n, m> result;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            result.m_matrix[i][j] = scale * m_matrix[i][j];
        }
    }

    return result;
}

template <int n, int m>
Matrix<n, m> Matrix<n ,m>::operator + (const Matrix<n, m> &other) const
{
    Matrix<n, m> result;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            result.m_matrix[i][j] = m_matrix[i][j] + other.m_matrix[i][j];
        }
    }

    return result;
}

template <int n, int m>
Matrix<n, m> Matrix<n ,m>::operator - (const Matrix<n, m> &other) const
{
    Matrix<n, m> result;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            result.m_matrix[i][j] = m_matrix[i][j] - other.m_matrix[i][j];
        }
    }

    return result;
}

typedef Matrix<3, 1> Vector3;
typedef Matrix<4, 1> Vector4;



#endif
