#include "canvas.h"

Canvas::Canvas()
    : world()
{
    std::cout << "Hello\n";
    set_double_buffered(true);
    Glib::signal_timeout().connect( sigc::mem_fun(*this, &Canvas::on_timeout), 1000 / 30 );
    Mesh mesh = load_obj("teapot.obj");
    world.meshes.push_back(mesh);

    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();
}

Canvas::~Canvas()
{
}

bool Canvas::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    float dt = float( clock () - begin_time ) /  CLOCKS_PER_SEC;
    begin_time = clock();

    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    cr->translate(width / 2, height / 2);

    return true;
}


bool Canvas::on_timeout()
{
    queue_draw();
    return true;
}
