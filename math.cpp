#include "math.h"

template <>
const Matrix<4, 4> Matrix<4, 4>::rot_x(double theta)
{
    Matrix<4, 4> mat;

    mat.m_matrix[0][0] = 1;
    mat.m_matrix[1][0] = 0;
    mat.m_matrix[2][0] = 0;

    mat.m_matrix[0][1] = 0;
    mat.m_matrix[1][1] = cos(theta);
    mat.m_matrix[2][1] = sin(theta);

    mat.m_matrix[0][2] = 0;
    mat.m_matrix[1][2] = -sin(theta);
    mat.m_matrix[2][2] = cos(theta);

    return mat;
}

template <>
const Matrix<4, 4> Matrix<4, 4>::rot_y(double theta)
{
    Matrix<4, 4> mat;

    mat.m_matrix[0][0] = cos(theta);
    mat.m_matrix[1][0] = 0;
    mat.m_matrix[2][0] = -sin(theta);
                       
    mat.m_matrix[0][1] = 0;
    mat.m_matrix[1][1] = 1;
    mat.m_matrix[2][1] = 0;
                       
    mat.m_matrix[0][2] = sin(theta);
    mat.m_matrix[1][2] = 0;
    mat.m_matrix[2][2] = cos(theta);

    return mat;
}

template <>
const Matrix<4, 4> Matrix<4, 4>::rot_z(double theta)
{
    Matrix<4, 4> mat;

    mat.m_matrix[0][0] = cos(theta);
    mat.m_matrix[1][0] = sin(theta);
    mat.m_matrix[2][0] = 0;
                       
    mat.m_matrix[0][1] = -sin(theta);
    mat.m_matrix[1][1] = cos(theta);
    mat.m_matrix[2][1] = 0;
                       
    mat.m_matrix[0][2] = 0;
    mat.m_matrix[1][2] = 0;
    mat.m_matrix[2][2] = 1;

    return mat;
}

template <>
const Matrix<4, 4> Matrix<4, 4>::translation(const Matrix<3, 1> &vec)
{
    Matrix<4, 4> mat;
    mat.m_matrix[0][0] = 1;
    mat.m_matrix[1][1] = 1;
    mat.m_matrix[2][2] = 1;
    mat.m_matrix[3][3] = 1;

    mat.m_matrix[0][3] = vec.m_matrix[0][0];
    mat.m_matrix[1][3] = vec.m_matrix[1][0];
    mat.m_matrix[2][3] = vec.m_matrix[2][0];

    return mat;
}

template <>
const Matrix<4, 4> Matrix<4, 4>::scale(const Matrix<3, 1> &vec)
{
    Matrix<4, 4> mat;
    mat.m_matrix[0][0] = vec.m_matrix[0][0];
    mat.m_matrix[1][1] = vec.m_matrix[1][0];
    mat.m_matrix[2][2] = vec.m_matrix[2][0];
    mat.m_matrix[3][3] = 1;

    return mat;
}

