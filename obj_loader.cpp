#include "obj_loader.h"
#include <fstream>
#include <sstream>
#include <string>

Mesh load_obj(char *filename)
{
    std::vector<Triangle> triangles; 
    std::vector<Vector3> all_verts;
    std::ifstream infile(filename);

    for(std::string line; getline(infile, line); )
    {
        std::stringstream s(line);

        char type;
        s >> type;

        switch(type) {
            case 'v': {
                Vector3 vec(0, 0, 0);
                s >> vec.m_matrix[0][0] >> vec.m_matrix[1][0] >> vec.m_matrix[2][0];
                all_verts.push_back(vec); 
                break;
                      }
            case 'f':
                int f[3];
                s >> f[0] >> f[1] >> f[2];
                Vector3 verts[3];
                verts[0] = all_verts[f[0] - 1];
                verts[1] = all_verts[f[1] - 1];
                verts[2] = all_verts[f[2] - 1];
                triangles.push_back(Triangle(verts));
                break;
        }
    }

    return Mesh(triangles);
}
