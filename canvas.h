#ifndef GTKMM_EXAMPLE_CANVAS_H
#define GTKMM_EXAMPLE_CANVAS_H

#include <gtkmm/drawingarea.h>
#include <ctime>
#include <cmath>
#include <cairomm/context.h>
#include <glibmm/main.h>
#include <iostream>
#include "math.h"
#include "renderer.h"
#include "obj_loader.h"

class Canvas : public Gtk::DrawingArea
{
    public:
        Canvas();
        virtual ~Canvas();

    protected:
        //Override default signal handler:
        bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;

        bool on_timeout();

        clock_t begin_time = clock();

        World world;
};

#endif // GTKMM_EXAMPLE_CANVAS_H
