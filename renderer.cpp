#include "renderer.h"

Triangle::Triangle(Vector3 *verts)
{
    for (int i = 0; i < 3; i++) {
        this->vertices[i] = verts[i];
    }
}

Triangle::~Triangle()
{
}

void Triangle::print()
{
    for (int i = 0; i < 3; i++) {
        vertices[i].print();
        std::cout << std::endl;
    }
}


Mesh::Mesh(std::vector<Triangle> t)
    : triangles(t)
{
}

Mesh::~Mesh()
{
}

void Mesh::print()
{
    for (auto &triangle : triangles) {
        triangle.print();
    }
}

World::World()
{
}

World::~World()
{
}

double theta = 0;
double theta1 = 0;
double theta2 = 0;
double z = 0;
double size = 100;


void glu_perspective( 
        const float &angleOfView, 
        const float &imageAspectRatio, 
        const float &n, const float &f, 
        float &b, float &t, float &l, float &r) 
{ 
    float scale = tan(angleOfView * 0.5 * M_PI / 180) * n; 
    r = imageAspectRatio * scale, l = -r; 
    t = scale, b = -t; 
}

void set_projection_matrix(const float &b, const float &t, const float &l, const float &r, 
        const float &n, const float &f, Matrix<4, 4> &M) 
{ 
    M.m_matrix[0][0] = 2 * n / (r - l); 
    M.m_matrix[0][1] = 0; 
    M.m_matrix[0][2] = 0; 
    M.m_matrix[0][3] = 0; 

    M.m_matrix[1][0] = 0; 
    M.m_matrix[1][1] = 2 * n / (t - b); 
    M.m_matrix[1][2] = 0; 
    M.m_matrix[1][3] = 0; 

    M.m_matrix[2][0] = (r + l) / (r - l); 
    M.m_matrix[2][1] = (t + b) / (t - b); 
    M.m_matrix[2][2] = -(f + n) / (f - n); 
    M.m_matrix[2][3] = -1; 

    M.m_matrix[3][0] = 0; 
    M.m_matrix[3][1] = 0; 
    M.m_matrix[3][2] = -2 * f * n / (f - n); 
    M.m_matrix[3][3] = 0;
}

Renderer::Renderer(int width, int height, XcbDraw &draw) 
    : width(width), height(height), draw(draw)
{
    float angleOfView = 90; 
    float near = 0.1; 
    float far = 100; 
    float imageAspectRatio = width / (float)height; 
    float b, t, l, r; 
    glu_perspective(angleOfView, imageAspectRatio, near, far, b, t, l, r); 
    set_projection_matrix(b, t, l, r, near, far, projection); 
}

Renderer::~Renderer()
{

}

int i = 0;
void Renderer::render()
{
    draw.clear(50); 

    Vector4 v0(50 , 100, 0, 0);
    Vector4 v1(200 , 70, 0, 0);
    Vector4 v2(100 , 50, 0, 0);
    std::vector<Vector4> tritest = { v0, v1, v2 };
    //draw_triangle(tritest, 255);

    //SET_PIXEL(draw, WIDTH - 1, HEIGHT - 1, 255, 255, 255);

    Vector4 offset(WIDTH / 2 - 200, HEIGHT / 2, 200, 0);

    //draw_line(50, 100, 50, 255, 255, 255);

    theta += 0.01;
    theta1 += 0.005;
    theta2 += 0.015;
    //size += 0.05;
    //z += 0.05;

    Matrix<4, 4> rot_xyz = Matrix<4, 4>::rot_z(theta2) * Matrix<4, 4>::rot_y(theta1) * Matrix<4, 4>::rot_x(theta); 

    Matrix<4, 4> translation = Matrix<4, 4>::translation(Vector3(0, 0, z));

    Matrix<4, 4> scale = Matrix<4, 4>::scale(Vector3(size, size, size));

    Matrix<4, 4> trans = rot_xyz * scale * translation;

    Vector4 light(cos(theta1), 0.5, 0.5, 0);
    Vector4 light_norm = light * (1 / light.size());

    Vector4 look(0, 0, 1, 0);

    //std::cout << "Draw world" << std::endl;
    for (Mesh &mesh : world.meshes) {

        #pragma omp parallel for 
        for (auto &t : mesh.triangles) {
            Vector4 t0 = transform_vector(trans, t.vertices[0]);
            Vector4 t1 = transform_vector(trans, t.vertices[1]);
            Vector4 t2 = transform_vector(trans, t.vertices[2]);

            Vector4 vec1 = t1 - t0;
            Vector4 vec2 = t2 - t0;
            Vector4 normal = vec2.cross(vec1);
            Vector4 norm = normal * (1 / normal.size());

            double shade = std::max(0.0, norm.dot(light_norm));

            if (norm.dot(look) >= 0) {

                Vector4 vert0 = to_2d(projection, t0) + offset;
                Vector4 vert1 = to_2d(projection, t1) + offset;
                Vector4 vert2 = to_2d(projection, t2) + offset;
                
                std::vector<Vector4> vec = {vert0, vert1, vert2};
                draw_triangle(vec, shade * 255);
            }
        }

        offset[0] += 400;
    }
}

#define EDGE_FUNCTION(a, b, c) (c.x() - a.x()) * (b.y() - a.y()) - (c.y() - a.y()) * (b.x() - a.x())
#define EDGE_FUNCTION_P(a, b, Px, Py) (Px - a.x()) * (b.y() - a.y()) - (Py - a.y()) * (b.x() - a.x())

void Renderer::draw_triangle(std::vector<Vector4> &pts, uint8_t shade) { 
    Vector4 bboxmin(WIDTH - 1, HEIGHT - 1, 0, 0); 
    Vector4 bboxmax(0, 0, 0, 0); 
    Vector4 clamp(WIDTH - 1, HEIGHT - 1, 0, 0); 
    for (int i=0; i<3; i++) { 
        for (int j=0; j<2; j++) { 
            bboxmin[j] = std::max(0.0,      std::min(bboxmin[j], pts[i][j])); 
            bboxmax[j] = std::min(clamp[j], std::max(bboxmax[j], pts[i][j])); 
        } 
    } 

    Vector4 &p0 = pts[0];
    Vector4 &p1 = pts[1];
    Vector4 &p2 = pts[2];

    for (int x = bboxmin.x(); x <= (int)bboxmax.x(); x++) { 
        for (int y = bboxmin.y(); y <= (int)bboxmax.y(); y++) { 
            double area = EDGE_FUNCTION(p0, p1, p2);
            double w0 = EDGE_FUNCTION_P(p0, p1, x, y); 
            double w1 = EDGE_FUNCTION_P(p1, p2, x, y); 
            double w2 = EDGE_FUNCTION_P(p2, p0, x, y);

            if (w0 >= 0 && w1 >= 0 && w2 >= 0) {
                //w0 /= area; 
                w1 /= area; 
                w2 /= area;
                double z = p0.z() + w1 * (p1.z() - p0.z()) + w2 * (p2.z() - p0.z());

                if (draw.zbuf[ZPIX(x, y)] >= z) {
                    draw.zbuf[ZPIX(x, y)] = z;
                    SET_PIXEL(draw, x, y, shade, shade, shade);
                }
            }        
        } 
    } 
}

Vector4 transform_vector(Matrix<4, 4> &trans, Vector3 &vec)
{
    return trans * Vector4(vec);
}

Vector4 to_2d(Matrix<4, 4> &projection, Vector4 &vec)
{
    return vec;
}
