
#include "main_window.h"
#include <glibmm/main.h>
#include <canvas.h>
#include <iostream>
#include <unistd.h>
#include "math.h"
#include "xcb_draw.h"
#include "obj_loader.h"


int main (int argc, char *argv[])
{
    std::cout << "Starting\n";

    Matrix<3, 3> mat2;
    mat2.m_matrix[0][0] = 4;
    mat2.m_matrix[0][1] = 3;
    mat2.m_matrix[0][2] = 2;
    mat2.m_matrix[1][0] = 4;
    mat2.m_matrix[1][1] = 5;
    mat2.m_matrix[1][2] = 2;
    mat2.m_matrix[2][0] = 4;
    mat2.m_matrix[2][1] = 6;
    mat2.m_matrix[2][2] = 7;
    mat2.print();
    std::cout << std::endl;

    Matrix<2, 2> mat22;
    mat22.m_matrix[0][0] = 1;
    mat22.m_matrix[0][1] = 0;
    mat22.m_matrix[1][0] = -1;
    mat22.m_matrix[1][1] = 2;

    Matrix<2, 2> newmat22 = mat22;


    for (int i = 0; i < 9; i++) {
        newmat22 = newmat22 * mat22;
    }

    newmat22.print();

    Vector3 vec(3, 1, 0);
    vec.print();
    std::cout << std::endl;

    Matrix<3, 1> prod = mat2 * vec;
    prod.print();
    std::cout << std::endl;

    Vector3 vec2(0, 5, 1);
    vec2.print();
    std::cout << std::endl;

    std::cout << vec.dot(vec2) << std::endl << std::endl;

    Vector3 cross = vec.cross(vec2);
    cross.print();
    std::cout << std::endl;

    XcbDraw draw;
    Renderer renderer(WIDTH, HEIGHT, draw);
    //Mesh mesh = load_obj("teapot.obj");
    Mesh mesh2 = load_obj("cube.obj");

    //renderer.world.meshes.push_back(mesh);
    renderer.world.meshes.push_back(mesh2);

    for (int i = 0; i < WIDTH; i++) {
        for (int j = 0; j < HEIGHT; j++) {
            //SET_PIXEL(draw, i, j, std::rand() % 255, std::rand() % 255, std::rand() % 255);
        }
    }

    while(1) {
        usleep((float)(1000000 / (float)30));
        
        renderer.render();
        draw.draw();
    }

    return 0;
}
