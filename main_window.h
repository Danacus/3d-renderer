#ifndef GTKMM_EXAMPLE_MAIN_WINDOW_H
#define GTKMM_EXAMPLE_MAIN_WINDOW_H

#include <gtkmm/button.h>
#include <gtkmm/window.h>
#include "canvas.h"

class MainWindow : public Gtk::Window
{

    public:
        MainWindow();
        virtual ~MainWindow();

    protected:
        //Member widgets:
        Canvas m_canvas;

};

#endif
