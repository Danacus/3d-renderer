#include "main_window.h"
#include <iostream>

MainWindow::MainWindow() : m_canvas()  
{
    // Sets the border width of the window.
    set_border_width(0);

    std::cout << "Creating Window\n";

    add(m_canvas);
    m_canvas.show();
}

MainWindow::~MainWindow()
{
}
