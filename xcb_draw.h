#ifndef XCB_DRAW_H
#define XCB_DRAW_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <xcb/xcb.h>
#include <xcb/shm.h>
#include <xcb/xcb_image.h>

#include "math.h"

#define WIDTH 1920
#define HEIGHT 1080
#define PIX(X, Y) (((Y) * WIDTH + (X)) * 4)
#define ZPIX(X, Y) ((Y) * WIDTH + (X))
#define RED + 0
#define GREEN + 1
#define BLUE + 2

#define SET_PIXEL(DRAW, X, Y, R, G, B) \
    DRAW.data[PIX(X, Y)RED] = R; \
    DRAW.data[PIX(X, Y)GREEN] = G; \
    DRAW.data[PIX(X, Y)BLUE] = B;

class XcbDraw
{
    public:
        XcbDraw();
        ~XcbDraw();
        void draw();
        void clear(uint8_t shade);
        uint8_t*                data;
        int16_t*               zbuf;

    private:
        xcb_connection_t*       connection;
        xcb_window_t            window;
        xcb_screen_t*           screen;
        xcb_gcontext_t          gcontext;
        xcb_generic_event_t*    event;
        xcb_shm_segment_info_t  info;
        xcb_pixmap_t            pix;
};

#endif
